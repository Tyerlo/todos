# Display todos from json placeholder

This is a coding test is designed to be simple and not very strict. This should give the consumer the opportunity to showcase coding style, file architecture and creative solutions.
<br/>

## 🚀 Getting started

Install dependencies:

```
npm i
```

Start development environment:

```
npm start
```

<br/>

## 💻 The tasks

Fork this repository, complete the tasks below. When you are happy with your solution you send a link to your forked repository.

### Get data from url

Get data from this url: https://jsonplaceholder.typicode.com/todos

### How many completed

Show how many of the total todos that are completed.

### Display todos

- The first 10 todos with an odd ID
- Each todo should show:
  - Title with the text reversed
  - If the todo is completed or not
    <br/>

## 🥅 Goals

### Maintainable

In real life clients will always change the specification. Be as prepared for this as you can.

### Reusable

If you think something could possibly be useful later, make it reusable.

### Readable

Others will have to read your code. Show your idea of _clean_ code.
