import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { App } from './app'
import { Global } from '@emotion/react'
import { resetGlobal } from './style/reset'
import 'bootstrap/dist/css/bootstrap.min.css';
ReactDOM.render(
  <React.StrictMode>
    <Global styles={[resetGlobal]} />
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
