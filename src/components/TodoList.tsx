import React from 'react'
import Todo from "../data/Todo.interface"

interface IProps {
  todo: Todo[],

}

function reverseString(result: Todo): React.ReactNode {
    return result.title.split("").reverse().join("");
}

function filterCompleted(todo: Todo[]) {
    return todo.filter(values => values.completed).length;
    
}

function showFirstTodos(todo: Todo[]) {
    return todo.slice(0, 20);
}

export const TodoList: React.FC<IProps> = ({ todo }) => {
    return (
        <ul className="list-group">
            {todo && showFirstTodos(todo).map(result => {      
                    return(
                    <li className="list-group-item"
                            key={result.id} >
                            {result.id % 2  ? 
                                <span>ID: {result.id}
                                   &nbsp;&nbsp;Title: {reverseString(result)}
                                   &nbsp;&nbsp;
                                    Completed: {result.completed ? "Yes" : "No"}
                                    &nbsp;&nbsp;                            
                                </span>
                                : null}
            </li>
    )
            })}
               Total completed: {filterCompleted(todo) }
        </ul>
    )
}
export default TodoList