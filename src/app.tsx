import React, { useEffect, useState } from 'react'
import { TodoList } from './components/TodoList';

const App = ()=> {
  
  const [todos, setTodos] = useState([]);

   async function fetchData() {
      const res = await  fetch("https://jsonplaceholder.typicode.com/todos")
     res.json()
       .then(res =>
         setTodos(res)
      )        
   }
  
 useEffect(() => {
    fetchData();
 }, []);
  
  return (<div>
   <TodoList todo={todos} />
  </div>
  )
}

export { App }
